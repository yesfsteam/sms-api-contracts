﻿using Yes.Infrastructure.Common.Models;

namespace Yes.Sms.Api.Contracts.Commands
{
    public class SendSmsCommand : JsonModel
    {
        /// <summary>
        /// Номер телефона абонента
        /// </summary>
        public string PnoneNumber { get; set; }
        
        /// <summary>
        /// Текст SMS
        /// </summary>
        public string Message { get; set; }
    }
}